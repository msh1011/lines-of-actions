#include <SFML/Graphics.hpp>
#include <unistd.h>
#include <iostream>
#include <map>
#include <sstream>
#include <set>
#include <thread>
#include <mutex>
#include <sys/time.h>

#include "node.hpp"
using namespace std;

#define BOARD_WITDH 800
#define TILE_SIZE BOARD_WITDH / 8
#define PEICE_R TILE_SIZE / 2.5
#define OFFSET (TILE_SIZE - 2 * PEICE_R) / 2


mutex  mut;
double prg = 0.0;
Move   last_m;
double last_w;
double last_v;

void                 UCT(State *rootstate, uint64_t itermax, uint64_t time_max) {
    mut.lock();
    Node *root = new Node(*rootstate,
                          { .from = 0, .to = 0 },
                          nullptr);
    mut.unlock();
    double roa = 0.0;
    int    rdi = 0;
    struct timeval tp;
    struct timeval timer;

    if (itermax == 0) {
        itermax = UINT64_MAX;
    }

    if (time_max == 0) {
        time_max = UINT64_MAX;
    } else {
        gettimeofday(&timer, NULL);
        time_max = timer.tv_sec + time_max;
    }
    uint64_t i = 0;


    while (i < itermax && timer.tv_sec < time_max) {
        Node *node = root;
        mut.lock();
        prg = 100 * i / itermax;
        State state = rootstate->copy();
        mut.unlock();

        while (node->untried.empty() && !node->children.empty()) {
            node = node->selectChild();
            Move m = node->getMove();
            state.move(m);
        }

        if (!node->untried.empty()) {
            Move m = node->untried[rand() % node->untried.size()];
            state.move(m);
            node = node->addChild(m, state);
        }

        uint8_t depth = 0;
        Move   *m     = state.getRandomMove();

        while (m != nullptr && depth < 150) {
            state.move(*m);
            m = state.getRandomMove();
            depth++;
        }

        while (node != nullptr) {
            uint8_t t = state.gameComplete(node->whiteMoved);
            node->update(t);
            node = node->parent;
        }
        i++;
        gettimeofday(&timer, NULL);
    }
    uint64_t maxV = 0;
    uint64_t maxI = 0;

    for (uint64_t i = 0; i < root->children.size(); i++) {
        if (root->children[i]->total > maxV) {
            maxV = root->children[i]->total;
            maxI = i;
        }
    }
    mut.lock();
    Move ms = root->children[maxI]->move;
    last_w = root->children[maxI]->wins;
    last_v = root->children[maxI]->total;
    last_m = ms;
    rootstate->move(ms);
    mut.unlock();
}

int main() {
    set < uint8_t > active;
    string world;
    struct timeval timer;
    gettimeofday(&timer, NULL);
    srand(timer.tv_sec);

    for (int i = -0; i < 8; i++) {
        string s;
        getline(cin, s);
        world += s;
    }
    State *s = new State(world);

#ifdef TIME_TEST
    for (int i = 100; i < 50000; i+= 100) {
        srand(1);
        struct timeval t1;
        struct timeval t2;
        State s1 = s->copy();
        gettimeofday(&t1, NULL);
        UCT(&s1, i, 0);
        gettimeofday(&t2, NULL);
        uint64_t t3 = (t2.tv_sec * 1000 + t2.tv_usec/1000) - (t1.tv_sec * 1000 + t1.tv_usec/1000);
        cout << i << "," << t3 << endl;
    }
    return 0;
#endif

#ifdef NOGUI
    int wi    = 0;
    double ti = 100;

    for (int t = 1; t <= ti; t++) {
        cout << t << "/" << ti << endl;
        int moves = 0;

        while (s->gameComplete(s->whiteMove) == 0) {
            if (!s->whiteMove) {
                UCT(s, 500, 0);
            } else {
                UCT(s, 100, 0);
            }
            moves++;
            cout << "Move: " << moves << '\r' << flush;
        }

        if (s->whiteMove) {
            cout << "X Wins       " << '\a' << endl;
            wi++;
        } else {
            cout << "O Wins       " << '\a' << endl;
        }
        s = new State(world);
    }
    cout << "X win rate: " << wi / ti << endl;
#else /* ifdef NOGUI */
    uint8_t mpos      = 0;
    uint8_t l_pos     = 0;
    bool    locked    = false;
    int     gamestate = 0;
    int     gametype  = 2;

    sf::Font font;
    font.loadFromFile("/usr/share/fonts/truetype/ubuntu/Ubuntu-C.ttf");

    sf::RenderWindow window(sf::VideoMode(BOARD_WITDH + 200, BOARD_WITDH),
                            "Lines of action");
    thread th = thread(printf, "");

    while (window.isOpen())
    {
        sf::Event event;

        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }

            if ((event.type == sf::Event::MouseButtonPressed) && (gamestate ==
                                                                  0)) {
                sf::Vector2i pos = sf::Mouse::getPosition(window);
                uint8_t mx       = (int)(pos.x / (TILE_SIZE + 0.0));
                uint8_t my       = (int)(pos.y / (TILE_SIZE + 0.0));

                if ((mx < 8) && (my < 8)) {
                    mpos = mx + my * 8;

                    if (!locked) {
                        l_pos  = mpos;
                        locked = true;
                    } else {
                        if (active.count(mpos) != 0) {
                            Move mv = { .from = l_pos, .to = mpos };
                            s->move(mv);
                            uint8_t t = s->gameComplete(!s->whiteMove);
                            cout << (int)l_pos << " -> " << (int)mpos << endl;
                            cout << (int)t << " " << s->whiteMove << endl;

                            if (t == 1) {
                                if (!s->whiteMove) {
                                    cout << "white wins" << endl;
                                } else {
                                    cout << "red wins" << endl;
                                }
                                gamestate = 1;
                                active.clear();
                            }

                            if (gametype != 2) {
                                th.join();
                                th = thread(UCT, s, 10000, 0);
                            }
                            printf("%lx \n%lx\n", s->currentBoard.white, s->currentBoard.red);
                        }
                        locked = false;
                    }
                }
            }
        }
        sf::Vector2i pos = sf::Mouse::getPosition(window);
        mut.lock();
        Board board       = s->getBoard();
        uint64_t progress = prg;

        if (!locked && (gamestate == 0) && (gametype != 0)) {
            uint8_t mx = (int)(pos.x / (TILE_SIZE + 0.0));
            uint8_t my = (int)(pos.y / (TILE_SIZE + 0.0));

            if ((mx < 8) && (my < 8)) {
                mpos               = mx + my * 8;
                vector < Move > mv = s->getMoves(mpos);
                active.clear();

                for (auto x : mv) {
                    active.insert(x.to);
                }
            } else {
                active.clear();
            }
        }

        mut.unlock();

        window.clear();
        sf::RectangleShape rect;
        rect.setSize(sf::Vector2f(progress * 2, 10));
        rect.setPosition(TILE_SIZE * 8, TILE_SIZE / 4);
        rect.setFillColor(sf::Color(0, 206, 0));
        sf::RectangleShape rect1;
        rect1.setSize(sf::Vector2f(TILE_SIZE * 2, 10));
        rect1.setPosition(TILE_SIZE * 8, TILE_SIZE / 4);
        rect1.setFillColor(sf::Color(128, 128, 128));
        window.draw(rect1);
        window.draw(rect);

        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                sf::RectangleShape rectangle;
                rectangle.setSize(sf::Vector2f(TILE_SIZE, TILE_SIZE));
                rectangle.setPosition(TILE_SIZE * x, TILE_SIZE * y);

                if ((x + y) % 2 == 0) {
                    rectangle.setFillColor(sf::Color(255, 206, 158));
                } else {
                    rectangle.setFillColor(sf::Color(209, 139, 71));
                }
                window.draw(rectangle);
                sf::Color active_color   = sf::Color(196, 0, 3);
                sf::Color inactive_color = sf::Color(255, 249, 244);

                if (active.count(x + y * 8) == 0) {
                    if ((board.white >> (y * 8 + x)) & 0x1) {
                        sf::CircleShape circle;
                        circle.setRadius(PEICE_R);
                        circle.setFillColor(inactive_color);
                        circle.setOutlineColor(sf::Color::Black);
                        circle.setOutlineThickness(5);
                        circle.setPosition(TILE_SIZE * x + OFFSET, TILE_SIZE * y
                                           + OFFSET);
                        window.draw(circle);
                    }

                    if ((board.red >> (y * 8 + x)) & 0x1) {

                        sf::CircleShape circle;
                        circle.setRadius(PEICE_R);
                        circle.setFillColor(active_color);
                        circle.setOutlineColor(sf::Color::Black);
                        circle.setOutlineThickness(5);
                        circle.setPosition(TILE_SIZE * x + OFFSET, TILE_SIZE * y
                                           + OFFSET);
                        window.draw(circle);
                    }
                } else if (gamestate == 0) {
                    sf::CircleShape circle;
                    circle.setRadius(PEICE_R);
                    circle.setOutlineColor(sf::Color::Black);
                    circle.setFillColor(sf::Color::Green);
                    circle.setOutlineThickness(3);
                    circle.setPosition(TILE_SIZE * x + OFFSET, TILE_SIZE * y +
                                       OFFSET);
                    window.draw(circle);
                }
            }
        }
        ostringstream ss;

        mut.lock();
        ss << " : " << (int)last_m.from << " -> " << (int)last_m.to << endl;
        mut.unlock();
        sf::Text text(ss.str(),
                      font);
        text.setPosition(800, 100);
        text.setCharacterSize(16);
        window.draw(text);
        window.display();
    }
#endif /* ifdef NOGUI */
}
