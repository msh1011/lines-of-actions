GNC = g++ -std=c++11

loa: 
	$(GNC) LOA.cpp node.cpp state.cpp -o loa -pthread -lsfml-window -lsfml-system -lsfml-graphics

no-gui: 
	$(GNC) LOA.cpp node.cpp state.cpp -o loa -DNOGUI -pthread -lsfml-window -lsfml-system -lsfml-graphics

timed:
	$(GNC) LOA.cpp node.cpp state.cpp -o loa -DTIME_TEST -pthread -lsfml-window -lsfml-system -lsfml-graphics
clean:
	rm -f loa 

run: clean loa
	./loa < default.map

test: clean no-gui
	@./loa < default.map
	@./loa < default.map
	@./loa < default.map
	@./loa < default.map
	@./loa < default.map
	@./loa < default.map
	@./loa < default.map
	@./loa < default.map
	@./loa < default.map
	@./loa < default.map
