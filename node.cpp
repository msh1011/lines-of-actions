#include <math.h>
#include "node.hpp"

Node::Node(State s, Move m, Node *n){
    move   = m;
    parent = n;
    state  = s;
    untried = s.getMoves();
    whiteMoved = s.whiteMove;
    total = 0;
    wins = 0;
}

Node* Node::addChild(Move m, State& s) {
    Node* n = new Node(s, m, this);
    children.push_back(n);
    for(uint64_t i = 0; i < untried.size(); i++){
        if(untried[i].from == m.from && untried[i].to == m.to){
            untried.erase(untried.begin()+i);
            break;
        }
    }
    return n;
}

Node* Node::selectChild(){
    double maxN;
    int maxI;
    double c = 2;
    for(uint64_t i = 0; i < children.size(); i++){
        double ucb1 = children[i]->wins / children[i]->total + c*sqrt(log(total)/children[i]->total);
        if(ucb1 > maxN){
            maxN = ucb1;
            maxI = i;
        }
    }
    return children[maxI];
}

void Node::update(uint8_t i){
    total += 1;
    wins += i;
}

Move Node::getMove(){
    return move;
}
