#include <stdint.h>
#include <mutex>
#include "state.hpp"

class Node {
public:

    Node(State, Move, Node *);
    Node* addChild(Move,
                  State&);
    Node* selectChild();
    void update(uint8_t);
    Move getMove();
    vector < Move > untried;
    vector < Node* > children;
    bool whiteMoved;
    Node *parent;
    State state;
    Move  move;
    uint64_t wins = 0;
    uint64_t total =0;
};
