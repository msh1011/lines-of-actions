#include <cstring>
#include <sstream>
#include <cstdlib>
#include "state.hpp"


State::State() {
    currentBoard.white = 0x0000000000000000;
    currentBoard.red   = 0x0000000000000000;
}

State::State(string s1) {
    currentBoard.white = 0x0000000000000000;
    currentBoard.red   = 0x0000000000000000;
    stringstream ss(s1);
    char c;

    for (int i = 0; i < 8; i++) {
        for (int l = 0; l < 8; l++) {
            ss >> c;

            if (c == 'X') {
                currentBoard.white |= (1UL << (i * 8 + l));
            } else if (c == 'O') {
                currentBoard.red |= (1UL << (i * 8 + l));
            }
        }
    }

    if (upper_cross.empty()) {
        init_static();
    }
}

void State::init_static() {
    for (int i = 0; i < 8; i++) {
        set < uint8_t > s1;

        for (int t = 0; t <= i; t++) {
            s1.insert(i + (t * 7));
        }

        upper_cross.push_back(s1);
    }

    for (int i = 0; i < 7; i++) {
        set < uint8_t > s1;

        for (int t = i * 8 + 15; t < 64; t += 7) {
            s1.insert(t);
        }
        upper_cross.push_back(s1);
    }

    for (int i = 0; i < 8; i++) {
        set < uint8_t > s1;

        for (int t = 0; t < 8 - i; t++) {
            s1.insert(i + (t * 9));
        }
        lower_cross.push_back(s1);
    }

    for (int i = 0; i < 7; i++) {
        set < uint8_t > s1;

        for (int t = i * 8 + 8; t < 64; t += 9) {
            s1.insert(t);
        }
        lower_cross.push_back(s1);
    }
}

State::State(State *s) {
    currentBoard.white = s->currentBoard.white;
    currentBoard.red   = s->currentBoard.red;
    whiteMove          = !s->whiteMove;
}

State State::copy() {
    State s;

    s.currentBoard.white = currentBoard.white;
    s.currentBoard.red   = currentBoard.red;
    s.whiteMove          = whiteMove;
    return s;
}

void State::move(Move mv) {
    uint64_t *current = &currentBoard.white;
    uint64_t *other   = &currentBoard.red;

    if (whiteMove) {
        other   = current;
        current = &currentBoard.red;
    }

    *current &= ~(1UL << mv.from);
    *current |= (1UL << mv.to);

    if ((*other >> mv.to) & 1UL) {
        *other &= ~(1UL << mv.to);
    }
    whiteMove = !whiteMove;
}

short State::getTurn() {
    return whiteMove;
}

Board State::getBoard() {
    return currentBoard;
}

void State::search_groups(uint64_t act, uint64_t i, bool visited[64]) {
    static int nums[] = { -9, -8, -7, -1, 1, 7, 8, 9 };

    visited[i] = true;

    for (int k = 0; k < 8; k++) {
        int  nt     = (int)i + nums[k];
        bool safe   = nt >= 0 && (nt < 64) && ((act >> nt) & 1UL) && !visited[nt];
        bool safe_a = (i % 8 == 0 && nt % 8 == 7) || (i % 8 == 7 && nt % 8 == 0);

        if (safe && !safe_a) {
            search_groups(act, i + nums[k], visited);
        }
    }
}

uint8_t State::gameComplete(bool turn) {
    int  count_i = 0;
    bool visited_i[64];

    memset(visited_i, 0, sizeof(visited_i));
    uint64_t *board = &currentBoard.white;

    if (!turn) {
        board = &currentBoard.red;
    }

    for (uint64_t i = 0; i < 64; ++i) {
        if (((*board >> i) & 1UL) && !visited_i[i]) {
            search_groups(*board, i, visited_i);
            count_i++;
        }
    }

    if (count_i == 1) {
        return 1;
    }
    return 0;
}

vector < Move > State::getMoves() {
    vector < Move > moves;

    if (gameComplete(whiteMove) == 1) {
        return moves;
    }
    uint64_t *active = &currentBoard.red;

    if (!whiteMove) {
        active = &currentBoard.white;
    }


    for (uint8_t x = 0; x < 64; x++) {
        if (((*active >> x) & 1UL) == 0) {
            continue;
        }

        generateXMoves(moves, x);
        generateYMoves(moves, x);
        generateZ1Moves(moves, x);
        generateZ2Moves(moves, x);
    }
    return moves;
}


vector < Move > State::getMoves(uint8_t x) {
    vector < Move > moves;

    if (gameComplete(whiteMove) == 1) {
        return moves;
    }
    uint64_t *active = &currentBoard.red;

    if (!whiteMove) {
        active = &currentBoard.white;
    }


    if (((*active >> x) & 1UL) == 0) {
        return moves;
    }
    generateXMoves(moves, x);
    generateYMoves(moves, x);
    generateZ1Moves(moves, x);
    generateZ2Moves(moves, x);
    return moves;
}

Move * State::getRandomMove() {
    if (gameComplete(whiteMove) == 1) {
        return nullptr;
    }
    uint64_t *active = &currentBoard.red;

    if (!whiteMove) {
        active = &currentBoard.white;
    }
    vector < Move > m;

    while (true) {
        uint8_t t = rand() % 64UL;

        while (((*active >> t) & 1UL) == 0) {
            t = rand() % 64;
        }
        m = getMoves(t);

        if (m.size() != 0) {
            break;
        }
    }

    Move *mv = new Move;
    *mv = m[rand() % m.size()];
    return mv;
}

void State::generateXMoves(vector < Move >& moves, uint8_t x) {
    uint8_t i = ((currentBoard.red | currentBoard.white) >> (x / 8) * 8) & 0xFF;

    uint8_t h1 = ((i >> 7) & 1) + ((i >> 6) & 1) + ((i >> 5) & 1) + (i >> 4 & 1) +
                 ((i >> 3) & 1) + ((i >> 2) & 1) + ((i >> 1) & 1) + (i & 1);

    bool valid_h1      = true;
    bool valid_h2      = true;
    uint64_t *active   = &currentBoard.red;
    uint64_t *inactive = &currentBoard.white;

    // Adjust for perspective
    if (!whiteMove) {
        active   = inactive;
        inactive = &currentBoard.red;
    }

    // check h1 in both directions
    for (uint8_t y = 1; y < h1; y++) {
        if (((x + y) < 64) && ((x + y) / 8 != x / 8)) {
            valid_h1 = false;
        }

        if (((x - y) < 64) && ((x - y) / 8 != x / 8)) {
            valid_h2 = false;
        }

        if (((*inactive >> (x + y)) & 1UL) != 0) {
            valid_h1 = false;
        }

        if (((*inactive >> (x - y)) & 1UL) != 0) {
            valid_h2 = false;
        }
    }

    // is the last tile the same color?
    if ((((*active >> (x + h1)) & 1UL) != 0) || ((x + h1) / 8 != x / 8)) {
        valid_h1 = false;
    }

    if (((*active >> (x - h1)) & 1UL) != 0 || ((x - h1) / 8 != x / 8)) {
        valid_h2 = false;
    }

    // add moves
    if (valid_h1) {
        moves.push_back({ x, (uint8_t)(x + h1), 'X' });
    }

    if (valid_h2) {
        moves.push_back({ x, (uint8_t)(x - h1), 'x' });
    }
}

void State::generateYMoves(vector < Move >& moves, uint8_t x) {
    uint8_t  i     = 0;
    uint64_t board = currentBoard.red | currentBoard.white;

    bool valid_h1      = true;
    bool valid_h2      = true;
    uint64_t *active   = &currentBoard.red;
    uint64_t *inactive = &currentBoard.white;

    // Adjust for perspective
    if (!whiteMove) {
        active   = inactive;
        inactive = &currentBoard.red;
    }

    // Count # tiles in the same colomn
    for (uint8_t w = x % 8; w < 64; w += 8) {
        i = (i << 1) | ((board >> w) & 1UL);
    }
    uint8_t h1 = ((i >> 7) & 1) + ((i >> 6) & 1) + ((i >> 5) & 1) + (i >> 4 & 1) +
                 ((i >> 3) & 1) + ((i >> 2) & 1) + ((i >> 1) & 1) + (i & 1);

    // check h1 in both directions
    for (uint8_t y = 1; y < h1; y++) {
        if (x + y * 8 > 63) {
            valid_h1 = false;
        }

        if (((*inactive >> (x + y * 8)) & 1UL) != 0) {
            valid_h1 = false;
        }

        if (x - y * 8 < 0) {
            valid_h2 = false;
        }

        if (((*inactive >> (x - y * 8)) & 1UL) != 0) {
            valid_h2 = false;
        }
    }

    // is the last tile the same color?
    if (((*active >> (x + 8 * h1)) & 1UL) != 0) {
        valid_h1 = false;
    }


    if (((*active >> (x - 8 * h1)) & 1UL) != 0) {
        valid_h2 = false;
    }

    // add moves
    if (valid_h1) {
        moves.push_back({ x, (uint8_t)(x + 8 * h1), 'Y' });
    }

    if (valid_h2) {
        moves.push_back({ x, (uint8_t)(x - 8 * h1), 'y' });
    }
}

void State::generateZ1Moves(vector < Move >& moves, uint8_t x) {
    uint8_t   i        = 0;
    uint64_t  board    = currentBoard.red | currentBoard.white;
    bool      valid    = true;
    uint64_t *active   = &currentBoard.red;
    uint64_t *inactive = &currentBoard.white;

    if (!whiteMove) {
        active   = inactive;
        inactive = &currentBoard.red;
    }


    set < uint8_t > s1;

    for (auto t : upper_cross) {
        if (t.count(x) != 0) {
            s1 = t;
            break;
        }
    }
    vector < uint8_t > i1;
    vector < uint8_t > i2;

    for (auto t : s1) {
        i += (board >> t) & 1UL;

        if (t < x) {
            i1.push_back(t);
        } else if (t > x) {
            i2.push_back(t);
        }
    }

    if (i1.size() >= i) {
        auto it = i1.rbegin();

        for (int t = 0; t < i - 1; t++, it++) {
            if (((*inactive >> (*it)) & 1UL) != 0) {
                valid = false;
                break;
            }
        }

        if (((*active >> *it) & 1UL) != 0) {
            valid = false;
        }

        if (valid) {
            moves.push_back({ x, (uint8_t)(*it), 'U' });
        }
    }
    valid = true;

    if (i2.size() >= i) {
        auto it = i2.begin();

        for (int t = 0; t < i - 1; t++, it++) {
            if (((*inactive >> (*it)) & 1UL) != 0) {
                valid = false;
                break;
            }
        }

        if (((*active >> *it) & 1UL) != 0) {
            valid = false;
        }

        if (valid) {
            moves.push_back({ x, (uint8_t)(*it), 'u' });
        }
    }
}

void State::generateZ2Moves(vector < Move >& moves, uint8_t x) {
    uint8_t   i        = 0;
    uint64_t  board    = currentBoard.red | currentBoard.white;
    bool      valid    = true;
    uint64_t *active   = &currentBoard.red;
    uint64_t *inactive = &currentBoard.white;

    if (!whiteMove) {
        active   = inactive;
        inactive = &currentBoard.red;
    }


    set < uint8_t > s1;

    for (auto t : lower_cross) {
        if (t.count(x) != 0) {
            s1 = t;
            break;
        }
    }
    vector < uint8_t > i1;
    vector < uint8_t > i2;

    for (auto t : s1) {
        i += (board >> t) & 1UL;

        if (t > x) {
            i1.push_back(t);
        } else if (t < x) {
            i2.push_back(t);
        }
    }

    if (i1.size() >= i) {
        auto it = i1.begin();

        for (int t = 0; t < i - 1; t++, it++) {
            if (((*inactive >> (*it)) & 1UL) != 0) {
                valid = false;
                break;
            }
        }

        if (((*active >> *it) & 1UL) != 0) {
            valid = false;
        }

        if (valid) {
            moves.push_back({ x, (uint8_t)(*it), 'L' });
        }
    }
    valid = true;

    if (i2.size() >= i) {
        auto it = i2.rbegin();
        valid = true;

        for (int t = 0; t < i - 1; t++, it++) {
            if (((*inactive >> (*it)) & 1UL) != 0) {
                valid = false;
                break;
            }
        }

        if (((*active >> *it) & 1UL) != 0) {
            valid = false;
        }

        if (valid) {
            moves.push_back({ x, (uint8_t)(*it), 'l' });
        }
    }
}
