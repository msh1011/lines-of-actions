#include <iostream>
#include <set>
#include <vector>
#include <string>
#include <stdint.h>
using namespace std;

struct Board {
    uint64_t white;
    uint64_t red;
};

struct Move {
    uint8_t from;
    uint8_t to;
    char    m;
};

static vector < set < uint8_t >> upper_cross;
static vector < set < uint8_t >> lower_cross;

class State {
public:

    State();
    State(string);
    State(State *);
    State   copy();
    void    move(Move);
    vector < Move >   getMoves();
    vector < Move >   getMoves(uint8_t);
    Move  * getRandomMove();
    uint8_t gameComplete(bool);
    short   getTurn();
    Board   getBoard();

    Board currentBoard;
    static string world;
    bool  whiteMove = false;

private:

    void generateXMoves(vector < Move >&,
                        uint8_t);
    void generateYMoves(vector < Move >&,
                        uint8_t);
    void generateZ1Moves(vector < Move >&,
                         uint8_t);
    void generateZ2Moves(vector < Move >&,
                         uint8_t);
    void init_static();
    void search_groups(uint64_t,
                       uint64_t,
                       bool[64]);

    friend ostream& operator << (ostream & out, const State &c)
            {
            string s1 = "\033[33mO\033[0m ";
            string s2 = "\033[31mO\033[0m ";
            s1 = "X ";
            s2 = "O ";

            for (int x = 0; x < 64; x++) {
                if ((c.currentBoard.white >> x) & 0x1) {
                    cout << s1;
                } else if ((c.currentBoard.red >> x) & 0x1) {
                    cout << s2;
                } else {
                    cout << "_ ";
                }

                if ((x + 9) % 8 == 0) {
                    cout << endl;
                }
            }
            out << endl;
            return out;
        }
};
